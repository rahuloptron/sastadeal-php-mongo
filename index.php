<?php
require 'vendor/autoload.php';

require 'config.php';

$database = $collection->sastadeals;
$collection = $database->product_info;

$getdata = $collection->find();

$getdata2 = $collection->find();

$i = 0;
$count = 5;
?>

<!doctype html>
<html lang="en">
<head>
<!-- Basic page needs
============================================ -->
<title>ShopMe | Home</title>
<meta charset="utf-8">
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">

<!-- Mobile specific metas
============================================ -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicon
============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="images/fav_icon.ico">

<!-- Google web fonts
============================================ -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

<!-- Libs CSS
============================================ -->
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/fontello.css">
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Theme CSS
============================================ -->
<link rel="stylesheet" href="js/rs-plugin/css/settings.css">
<link rel="stylesheet" href="js/owlcarousel/owl.carousel.css">
<link rel="stylesheet" href="js/arcticmodal/jquery.arcticmodal.css">
<link rel="stylesheet" href="css/style.css">

<!-- JS Libs
============================================ -->

<!-- Old IE stylesheet
============================================ -->
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="css/oldie.css">
<![endif]-->
</head>
<body class="front_page">



<!-- - - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->

<div class="wide_layout">

<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

<header id="header" class="type_5">
<!-- - - - - - - - - - - - - - Bottom part - - - - - - - - - - - - - - - - -->
<div class="bottom_part">
<div class="container">
<div class="row">
<div class="main_header_row">
<div class="col-sm-3">
<!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->
<a href="index.php" class="logo">
<img src="../images/logo.png" alt="">
</a>
<!-- - - - - - - - - - - - - - End of logo - - - - - - - - - - - - - - - - -->
</div><!--/ [col]-->
<div class="col-lg-6 col-md-5 col-sm-5">
</div><!--/ [col]-->
<div class="col-lg-3 col-sm-4">
<div class="clearfix">
<!-- - - - - - - - - - - - - - Language change - - - - - - - - - - - - - - - - -->

<div class="call_us alignright">
<span>Call us:</span> <b> +91 9321189090</b>


</div><!--/ .alignright.site_settings-->

</div><!--/ .clearfix-->

</div><!--/ [col]-->
</div><!--/ .main_header_row-->
</div><!--/ .row-->
</div><!--/ .container-->
</div><!--/ .bottom_part -->
</header>


<div class="page_wrapper">

	

<div class="container">

<section class="section_offset animated transparent" data-animation="fadeInDown"> 

<h3 class="section_title">Today's Deals</h3>

<div class="tabs type_2 products">


<div class="tab_containers_wrap">

<div class="tab_container">

<!-- - - - - - - - - - - - - - Carousel of today's deals - - - - - - - - - - - - - - - - -->

<div class="owl_carousel carousel_in_tabs type_2">

<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

<?php foreach($getdata as $data){ ?>

   <?php if($i==5) break; ?>
   

<div class="product_item">

<div class="image_wrap">

<a href="<?php echo $data["url"] . "\n"; ?>"><img src="../images<?php echo $data["images"]->image1 ?>" alt=""></a>

</div><!--/. image_wrap-->

<div class="description">

<p><a href="<?php echo $data["url"] . "\n"; ?>"><?php echo $data["title"] . "\n"; ?></a></p>

<div class="clearfix product_info">

<p class="product_price alignleft"><s><?php echo $data["marketprice"] . "\n"; ?></s> <b><?php echo $data["offerprice"] . "\n"; ?></b></p>

</div><!--/ .clearfix.product_info-->

</div>


</div><!--/ .product_item-->

<?php $i++;   ?>


<?php } ?>
</div><!--/ .owl_carousel-->

</div><!--/ #tab-1-->



</div>

</div>

</section><!--/ .section_offset-->



<section class="section_offset animated transparent" data-animation="fadeInDown"> 

<h3 class="section_title">Popular Products</h3>

<div class="tabs type_2 products">



<div class="tab_containers_wrap">

<div class="tab_container">

<!-- - - - - - - - - - - - - - Carousel of today's deals - - - - - - - - - - - - - - - - -->

<div class="owl_carousel carousel_in_tabs type_2">

<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->
<?php foreach($getdata2 as $value){ ?>

   <?php if($count==10) break; ?>
   

<div class="product_item">

<div class="image_wrap">

<a href="<?php echo $value["url"] . "\n"; ?>"><img src="../images<?php echo $value["images"]->image1 ?>" alt=""></a>

</div><!--/. image_wrap-->

<div class="description">

<p><a href="<?php echo $value["url"] . "\n"; ?>"><?php echo $value["title"] . "\n"; ?></a></p>

<div class="clearfix product_info">

<p class="product_price alignleft"><s><?php echo $value["marketprice"] . "\n"; ?></s> <b><?php echo $value["offerprice"] . "\n"; ?></b></p>

</div><!--/ .clearfix.product_info-->

</div>


</div><!--/ .product_item-->

<?php $count++;   ?>

<?php } ?>


</div><!--/ .owl_carousel-->


</div><!--/ #tab-1-->



</div>

</div>

</section><!--/ .section_offset-->



</div><!--/ .container-->

</div><!--/ .page_wrapper-->

<?php include "includes/footer-3.php" ?>



</div><!--/ [layout]-->

<!-- Include Libs & Plugins
============================================ -->
<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/queryloader2.js"></script>
		<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="js/jquery.appear.js"></script>
		<script src="js/owlcarousel/owl.carousel.min.js"></script>
		<script src="js/royalslider/jquery.easing-1.3.js"></script>
		<script src="js/jquery.countdown.plugin.min.js"></script>
		<script src="js/jquery.countdown.min.js"></script>
		<script src="js/arcticmodal/jquery.arcticmodal.js"></script>
		<script src="js/jquery.tweet.min.js"></script>
		<script src="js/colorpicker/colorpicker.js"></script>
		<script src="js/retina.min.js"></script>
		<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>

		<!-- Theme files
		============================================ -->
		<script src="js/theme.plugins.js"></script>
		<script src="js/theme.core.js"></script>

</body>
</html>