<aside class="col-md-3 col-sm-4">

							
                            <section class="section_offset "><section class="widget">

									<h4>Why Customers Choose Us</h4>

									<ul class="list_of_infoblocks theme_box">

										<li>
											<i class="icon-thumbs-up-1"></i>
											<h6>The Highest Product Quality</h6>
											<p>We sell only best qualiy products from certified vendors at best price</p>
										</li>

										<li>
											<i class="icon-paper-plane"></i>
											<h6>Fast &amp; Easy Delivery</h6>
											<p>Our products are delivered much faster compared to other sellers </p>
										</li>

										<li>
											<i class="icon-lock"></i>
											<h6>Safe &amp; Secure Payment</h6>
											<p>Don't worry. We have most secure payment gateway to accept payments</p>
										</li>

										

									</ul>

								</section> </section>

							<section class="section_offset">

								<h3>Seller Information</h3>

								<div class="theme_box">

									<div class="seller_info clearfix">

										

										<div class="wrapper">

											<a href="#"><b>Kamal Imaging</b></a>

											<p class="seller_category">Mumbai (India)</p>

										</div>

									</div><!--/ .seller_info-->

									<ul class="seller_stats">

										
 		

										<li><span class="bold">99.8%</span> Positive Feedback</li>

										<li><span class="bold">7606</span> Transactions</li>

									</ul>

									<div class="v_centered">

										<a href="#" class="button_blue mini_btn">Call : +91 9321189090</a>

										
									</div>

								</div><!--/ .theme_box -->

								
							</section>

							<section class="section_offset">

								<h3>Bank Details</h3>

								<div class="theme_box">

									<div class="seller_info clearfix">

										

									</div><!--/ .seller_info-->

									<ul class="seller_stats">

						<li><span class="bold">A/c Name  :</span>KAMAL IMAGING		</li>
							<li><span class="bold">A/c Type :</span>Current Account</li>
							<li><span class="bold">Account No :</span> 11208430000066</li>

							<li><span class="bold">Bank Name :</span>Bank Of Baroda</li>

							<li><span class="bold">Branch   :</span>Goregaon (East), Station</li>

 							
							
							<li><span class="bold">RTGS/NEFT IFSC:</span> HDFC0001120</li>

						</ul>

									

								</div><!--/ .theme_box -->

								
							</section>


						</aside>