<?php
require 'vendor/autoload.php';
$collection = (new MongoDB\Client("mongodb://192.168.1.110:27017"));

$database = $collection->sastadeals;


$collection = $database->product_info;

//$specData = $database->specification;

//$reviewData = $database->reviews;

$chooseData = $database->choose_us;

$bankData = $database->bank_details;

$sellerData = $database->seller_info;

//$imagesData = $database->images;

//$id = $_GET['_id'];

//$getdata = $collection->find(["_id" => new MongoDB\BSON\ObjectID($id)]);

$url =  $_SERVER['REQUEST_URI'];

$getdata = $collection->find(["url" => $url]);


//$getSpec = $specData->find();

//$getReviews = $reviewData->find();

$getChoose = $chooseData->find();

$getBank = $bankData->find();

$getSeller = $sellerData->find();

//$getImages = $imagesData->find();


?>

<!doctype html>
<html lang="en">
<head>
	<?php foreach ($getdata as $value) { ?>
<title><?php echo $value["title"] . "\n"; ?></title>


<meta charset="utf-8">
<meta name="author" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Favicon
============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="images/fav_icon.ico">
<!-- Google web fonts
============================================ -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../css/animate.css">
<link rel="stylesheet" href="../css/fontello.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">

<!-- Theme CSS
============================================ -->
<link rel="stylesheet" href="../js/fancybox/source/jquery.fancybox.css">
<link rel="stylesheet" href="../js/fancybox/source/helpers/jquery.fancybox-thumbs.css">
<link rel="stylesheet" href="../js/arcticmodal/jquery.arcticmodal.css">
<link rel="stylesheet" href="../js/owlcarousel/owl.carousel.css">
<link rel="stylesheet" href="../css/watches.css">
<!-- JS Libs
============================================ -->

<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

-->
</head>
<body>
<div class="wide_layout">
<header id="header" class="type_5">
<!-- - - - - - - - - - - - - - Bottom part - - - - - - - - - - - - - - - - -->
<div class="bottom_part">
<div class="container">
<div class="row">
<div class="main_header_row">
<div class="col-sm-3">
<!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->
<a href="index2.php" class="logo">
<img src="../images/logo.png" alt="">
</a>
<!-- - - - - - - - - - - - - - End of logo - - - - - - - - - - - - - - - - -->
</div><!--/ [col]-->
<div class="col-lg-6 col-md-5 col-sm-5">
</div><!--/ [col]-->
<div class="col-lg-3 col-sm-4">
<div class="clearfix">
<!-- - - - - - - - - - - - - - Language change - - - - - - - - - - - - - - - - -->

<div class="call_us alignright">
<span>Call us:</span> <b> +91 9321189090</b>


</div><!--/ .alignright.site_settings-->

</div><!--/ .clearfix-->

</div><!--/ [col]-->
</div><!--/ .main_header_row-->
</div><!--/ .row-->
</div><!--/ .container-->
</div><!--/ .bottom_part -->
</header>
<div class="secondary_page_wrapper">
<div class="container">

<div class="row">
<main class="col-md-9 col-sm-8">


<section class="section_offset">
<div class="theme_box clearfix">
<div class="clearfix">

<div class="single_product">
<!-- - - - - - - - - - - - - - Image preview container - - - - - - - - - - - - - - - - -->
<div class="image_preview_container">

<img id="img_zoom" data-zoom-image="../images<?php echo $value["images"]->image1 ?>" src="../images<?php echo $value["images"]->image1 ?>" alt="">


<button class="button_grey_2 icon_btn middle_btn open_qv"><i class="icon-resize-full-6"></i></button>
</div><!--/ .image_preview_container-->

<div class="product_preview">
<div class="owl_carousel" id="thumbnails">	

	<?php 

foreach($value["images"] as $data){ ?>

<a href="#" data-image="../images<?php echo $data ?>" data-zoom-image="../images<?php echo $data ?>">
<img src="../images<?php echo $data ?>" data-large-image="../images<?php echo $data ?>" alt="">
</a>

<?php } ?>
</div><!--/ .owl-carousel-->
</div><!--/ .product_preview-->


</div>



<div class="single_product_description">
<h3 class="offset_title"><?php echo $value["title"] . "\n"; ?></h3>
<hr>
<p class="product_price">Offer Price: <b class="theme_color"><?php echo $value["offerprice"] . "\n"; ?></b></p>
<div class="description_section">
<table class="product_info">
<tbody>
<tr>
<td>Market Price: </td>
<td><?php echo $value["marketprice"] . "\n"; ?></td>
</tr>

</tbody>
</table>
</div>
<hr>
<div class="description_section">
<ul class="list_type_7">


<?php

foreach($value["Points"] as $data){ ?>
<li> <?php echo $data; ?></li>
<?php } 
?>
</ul>

</div>
<hr>


<button class="button_blue huge_btn" type="button" id="btnShow" />Call : 9321189090 </button>


</div>

</div>

</div>
</section>
<?php include "includes/adsense1.php" ?>
<div class="section_offset">

<div class="theme_box clearfix">

<div class="tab_containers_wrap">
<h3>Description</h3>
<p><?php echo $value["description"] . "\n"; ?></p>																						
</div>
</div>

</div>

<section class="section_offset">
<div class="theme_box clearfix">

<h2>Specifications</h2>

<ul class="specifications">

<?php

foreach($value["spec"] as $key => $data){ ?>
<li><span><?php echo $key . "\n"; ?></span><?php echo $data . "\n"; ?></li>
<?php } 
?>




</div>

</section>

</section>


<section class="section_offset theme_box">
<h3>Customer Reviews (Top 3)</h3>



<ul class="reviews">

<?php foreach($value["review"] as $data){ ?>

<li>
<article class="review">

<div class="review-body">
<div class="review-meta">

<h5 class="bold"><?php echo $data->title . "\n"; ?></h5>
<ul class="rating">
<li class="active"></li>
<li class="active"></li>
<li class="active"></li>
<li class="active"></li>
<li class="active"></li>
</ul>
Review by <a href="#" class="bold"><?php echo $data->name . "\n"; ?></a> on <?php echo $data->date . "\n"; ?>
</div>
<p><?php echo $data->desc . "\n"; ?></p>
</div>
</article>
</li>

<?php } ?>
</ul>

</section>



</main>
<aside class="col-md-3 col-sm-4">


<section class="section_offset "><section class="widget">

<h4>Why Customers Choose Us</h4>

<ul class="list_of_infoblocks theme_box">

<?php 
foreach ($getChoose as $data) {

foreach($data["choose_us"] as $value){ ?>

<li>
<i class="icon-thumbs-up-1"></i>
<h6><?php echo $value["title"] . "\n"; ?></h6>
<p><?php echo $value["description"] . "\n"; ?></p>
</li>

<?php } ?>

<?php } ?>

</ul>

</section> </section>

<section class="section_offset">

<h3>Seller Information</h3>

<div class="theme_box">



<ul class="seller_stats">
<?php 
foreach ($getSeller as $data) {

foreach($data["seller"] as $value){ ?>



<li><span class="bold"><?php echo $value ?></span></li>



<?php } ?>
<?php } ?>

</ul>

<div class="v_centered">

<a href="#" class="button_blue mini_btn">Call : +91 9321189090</a>


</div>

</div><!--/ .theme_box -->


</section>

<section class="section_offset">

<h3>Bank Details</h3>

<div class="theme_box">

<div class="seller_info clearfix">



</div><!--/ .seller_info-->

<ul class="seller_stats">

<?php	foreach ($getBank as $data) {

foreach($data["account"] as $key => $value){ ?>

<li><span class="bold"><?php echo $key; ?>  :</span><?php echo $value; ?></li>

<?php } ?>
<?php } ?>

</ul>



</div><!--/ .theme_box -->


</section>


</aside>
</div>


</div>
</div>

<hr>
<?php include "includes/footer-3.php" ?>

</div>


<script src="../js/jquery-2.1.1.min.js"></script>
<script src="../js/queryloader2.js"></script>
<script src="../js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="../js/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="../js/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
<script src="../js/owlcarousel/owl.carousel.min.js"></script>
<script src="../js/jquery.tweet.min.js"></script>
<!-- Theme files
============================================ -->
<script src="../js/theme.plugins.js"></script>
<script src="../js/theme.core.js"></script>
</body>
</html>